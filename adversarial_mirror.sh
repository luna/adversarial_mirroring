#!/bin/sh
set -eux
repo_url=$1
target_remote=$2
main_branch=$3
author="Adversary <amongus@example.org>"

repo_name=$(printf "%s" "$repo_url" | rev | cut -d '/' -f1 | rev | cut -d '.' -f1)
echo "$repo_name"
if [ ! -d "$repo_name" ]; then
  echo "cloning..."
  git clone "$repo_url"
fi

# already exists, fetch and copy to adversarial
git -C "$repo_name" fetch --force origin
adversarial="adversarial-$repo_name"
mkdir -p "$adversarial"

if [ ! -d "$adversarial/.git" ]; then
  git -C "$adversarial" init -b main
  git -C "$adversarial" remote add origin "$target_remote"
  git -C "$adversarial" config user.email amongus@example.org
  git -C "$adversarial" config user.name Adversary
fi

copy() {
  branch=$1
  branch_target=$2
  echo "copy $branch --> $branch_target"

  git -C "$repo_name" checkout "$branch"

  set +e
  git -C "$adversarial" rev-parse --verify "$branch_target"
  branch_exists_exitcode=$?
  set -e

  if [ "$branch_exists_exitcode" = "0" ]; then
    git -C "$adversarial" switch "$branch_target"
  else
    # always create from main lul
    if [ "$branch_target" = "$main_branch" ]; then
      git -C "$adversarial" checkout -b "$branch_target"
    else
      git -C "$adversarial" checkout "$main_branch"
      git -C "$adversarial" checkout -b "$branch_target"
    fi
  fi

  # copy, commit

  toplevels=$(find "$repo_name" -mindepth 1 -maxdepth 1 -not -path "$repo_name/.git")
  for path in $toplevels; do
    echo "$path"
    cp -vr "$path" "$adversarial"
  done
  timestamp=$(git -C "$repo_name" log -1 --format="%at" | xargs -I{} date -d @{} +%Y-%m-%dT%H:%M:%S)
  message=$(git -C "$repo_name" log -1 --format="%s")
  remote_author=$(git -C "$repo_name" log -1 --format="%cn")
  remote_author_email=$(git -C "$repo_name" log -1 --format="%ce")

  cd "$adversarial"
  git add .
  set +e
  git commit --author "$author" -m "$branch_target - $message" -m "made at remote:$timestamp" -m "taken at now:$(date +%Y-%m-%dT%H:%M:%S)" -m "made by remote $remote_author <$remote_author_email>"
  set -e
  cd ..
}

# always copy main first so that other branches can be made from main
copy "origin/$main_branch" "$main_branch"

for line in $(git -C "$repo_name" for-each-ref --format='%(refname)' refs/remotes/origin); do
  branch=$(printf "%s" "$line" | sed 's/refs\/remotes\///g')
  branch_target=$(printf "%s" "$branch" | sed 's/origin\///g')
  echo "copying branch $branch  -> $branch_target"
  if [ "$branch_target" != "HEAD" ]; then
    copy "$branch" "$branch_target"
  else
    echo "skipping HEAD, not real branch"
  fi
done


echo "push time"
cd "$adversarial"
git push --all origin
