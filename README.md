# adversarial_mirroring

adversarial mirroring of repositories that might want to fight back

## notes

- dont use urls ending in `.git` the script will explode
- the way this works is by copying all files from one branch to the other
- that means u lose all git history. the price u pay for progress

pros:
- if the repo fights back they cant just force-push their changes away. they will forever stay in the mirror

# usage

```sh
./adverserial_mirror.sh source_git_repo target_git_repo main_branch
```

./daemon.sh https://github.com/discord/erlpack https://gitdab.com/luna/adverserial-discord-api-spec master